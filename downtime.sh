#!/bin/bash
logfile=downtime.log
interval=1
online=true
while true; do
    if ping -c 1 8.8.8.8 &> /dev/null
    then
        if ! $online
        then
            online=true
            echo "online - `date -u`" >> $logfile
        fi
    else
        if $online
        then
            online=false
            echo "offline - `date -u`" >> $logfile
        fi
    fi
    sleep $interval
done